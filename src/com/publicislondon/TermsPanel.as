import com.publicislondon.TermsScroll;
/**
 * Terms Button
 * @author Allandt Bik-Elliott
 */

import mx.utils.Delegate;

class com.publicislondon.TermsPanel extends MovieClip
{
	public var mcMask			: MovieClip;
	public var mcText			: MovieClip;
	public var mcBg				: MovieClip;
	public var mcTermsButton	: MovieClip;
	public var scroller			: TermsScroll;
	
	public function TermsPanel() 
	{
		_visible = false;
		
		onEnterFrame = init;
	}
	
	private function init():Void 
	{
		delete onEnterFrame;
		
		scroller.thumbPercent = mcMask._height / mcText._height;
		//scroller.addEventListener(TermsScroll.UPDATE, Delegate.create(this, handleScrollUpdate));
		onEnterFrame = handleScrollUpdate;
		
		mcText._visible = false;
		scroller._visible = false;
		mcBg._visible = false;
		
		mcTermsButton.onRollOver = Delegate.create(this, openPanel);
		mcTermsButton.onRelease = Delegate.create(this, closePanel);
		mcBg.onRollOut = Delegate.create(this, closePanel);
		
		_visible = true;
	}
	
	private function openPanel():Void
	{
		mcMask._visible = true;
		mcText._visible = true;
		scroller._visible = true;
		mcBg._visible = true;
		
		mcTermsButton.gotoAndStop(2);
	}
	
	private function closePanel():Void
	{
		mcMask._visible = false;
		mcText._visible = false;
		scroller._visible = false;
		mcBg._visible = false;
		
		mcTermsButton.gotoAndStop(1);
	}
	
	private function handleScrollUpdate(e:Object):Void
	{
		mcText._y = (-(mcText._height - mcMask._height) * scroller.percent) + mcMask._y;
	}
	
	
}