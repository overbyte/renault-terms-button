/**
 * Custom scroller for Renault Terms Button 
 * @author Allandt Bik-Elliott
 * 
 * 20140228 removed EventDispatcher to save ~1kb
 */

import mx.utils.Delegate;
//import mx.events.EventDispatcher;

class com.publicislondon.TermsScroll extends MovieClip
{
	//static public var UPDATE	: String = "termsUpdate";
	
	// onstage
	public var mcTrack	: MovieClip;
	public var mcThumb	: MovieClip;
	
	// 20140228 removed EventDispatcher to save ~1kb
	//public var addEventListener		: Function;
	//public var removeEventListener	: Function;
	//private var dispatchEvent		: Function;
	
	private var _thumbPc	: Number;
	private var _percent	: Number;
	private var _active		: Boolean = false;
	
	public function TermsScroll() 
	{
		//EventDispatcher.initialize(this);
		onEnterFrame = init;
	}
	
	public function enable():Void
	{
		mcThumb.onMouseDown = Delegate.create(this, handleMouseDown);
		_root.onMouseUp = Delegate.create(this, handleMouseUp);
	}
	
	public function disable():Void
	{
		delete mcThumb.onMouseDown;
		delete _root.onMouseUp;
		delete onEnterFrame;
	}
	
	private function init():Void 
	{
		delete onEnterFrame;
		
		enable();
	}
	
	private function handleMouseDown(e:Object):Void
	{
		onEnterFrame = Delegate.create(this, handleEnterFrame);
		
		mcThumb.startDrag(false, mcTrack._x, mcTrack._y, mcTrack._x, mcTrack._y + mcTrack._height - mcThumb._height + 1);
	}
	
	private function handleEnterFrame(e:Object):Void
	{
		_percent = mcThumb._y / (mcTrack._height - mcThumb._height);
		
		//dispatchEvent( { type:TermsScroll.UPDATE } );
	}
	
	private function handleMouseUp(e:Object):Void
	{
		delete onEnterFrame;
		mcThumb.stopDrag();
	}
	
	public function get thumbPercent():Number 
	{
		return _thumbPc;
	}
	public function set thumbPercent(value:Number):Void 
	{
		_thumbPc = value;
		
		if (_thumbPc > 1) 
		{
			// Hacky but we need visible
			_xscale = 0;
			_yscale = 0;
		}
		else 
		{
			mcThumb._yscale = _thumbPc * 100;
			_active = true;
		}
	}
	
	public function get percent():Number 
	{
		return _percent;
	}
	
	public function get active():Boolean 
	{
		return _active;
	}
	
}